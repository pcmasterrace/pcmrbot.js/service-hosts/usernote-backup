const {ServiceBroker} = require("moleculer");
const moment = require("moment");
const faker = require("faker");

const broker = new ServiceBroker({
	logFormatter: "simple",
	transporter: "TCP",
	namespace: "pcmrbotjsv2"
});

function generateNote() {
	return {
		userId: faker.internet.userName(),
		serviceType: "reddit",
		note: faker.lorem.sentence(),
		timestamp: faker.date.recent(),
		creatorId: faker.internet.userName(),
		creatorServiceType: "reddit"
	};
}

broker.createService({
	name: "test",
	actions: {
		addSingleUsernote: {
			name: "addSingleUsernote",
			handler(ctx) {
				const params = generateNote();
				this.logger.info("Creating new entry with the following parameters:", JSON.stringify(params, null, "\t"));
				this.broker.call("v2.accounts.usernotes.create", params);
			}
		},
		addMultipleUsernotes: {
			name: "addMultipleUsernotes",
			params: {
				count: {
					type: "number",
					optional: true,
					integer: true,
					min: 1,
					max: 20
				}
			},
			handler(ctx) {
				let notes = [];
				const count = ctx.params.count || Math.floor(Math.random() * 19) + 1;
				for (let i = 0; i < count; i++) {
					notes.push(generateNote());
				}
				this.logger.info("Creating the following new entries:", JSON.stringify(notes, null, "\t"));
				this.broker.call("v2.accounts.usernotes.create.bulk", {notes: notes});
			}
		},
		addMultipleUsernotesPredetermined: {
			name: "addTwoUsernotes",
			handler(ctx) {
				const notes = [
					{
						userId: "TheAppleFreak",
						serviceType: "reddit",
						note: "Test Usernote Please Ignore",
						timestamp: "2019-05-27T16:11:33Z",
						creatorId: "pedro19",
						creatorServiceType: "reddit"
					},
					{
						userId: "zeug666",
						serviceType: "reddit",
						note: "Test Usernote Please Ignore",
						timestamp: "2019-05-27T16:11:33Z",
						creatorId: "eegras",
						creatorServiceType: "reddit"
					},
					{
						userId: "TheAppleFreak",
						serviceType: "reddit",
						note: faker.lorem.sentence(),
						timestamp: faker.date.recent(),
						creatorId: "eegras",
						creatorServiceType: "reddit"
					},
				]
				this.logger.info("Creating the following new entries:", JSON.stringify(notes, null, "\t"));
				this.broker.call("v2.accounts.usernotes.create.bulk", {notes: notes});
			}
		}
	}
});

broker.start();
broker.repl();