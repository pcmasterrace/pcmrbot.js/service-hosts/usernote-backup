# Usernote Backup

This is a single purpose service host for creating a database of all Toolbox user notes from various revisions. This implements `reddit` in single-user mode, `toolbox`, and a Moleculer REPL for interfacing with the service. 